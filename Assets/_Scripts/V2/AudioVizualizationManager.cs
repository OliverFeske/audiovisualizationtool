﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using V2;

public class AudioVizualizationManager : MonoBehaviour
{
	[SerializeField] private List<Transform> aVIList;
	[SerializeField] private float timerMax;
	private float timer;
	private int a = 0;

	void Start()
	{
		foreach (Transform aVIObject in transform)
		{
			aVIList.Add(aVIObject);
		}
	}

	void Update()
	{
		timer += Time.deltaTime;
		if (timer > timerMax)
		{
			if (a < aVIList.Count)
			{
				aVIList[a].gameObject.SetActive(true);
				aVIList[a].GetComponent<AudioVisualizationItem>().Init();
				a++;
				timer = 0f;
			}
		}
	}
}

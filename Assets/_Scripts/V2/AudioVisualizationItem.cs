﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace V2
{
	public class AudioVisualizationItem : MonoBehaviour
	{
		#region Enums
		//private enum VisualizationState { FrequencyBands, AudioBandBuffer }
		//private enum Channel { Stereo, Left, Right }
		//public enum FactorsOfTwo { Two, Four, Eight, Sixteen, ThirtyTwo, SixtyFour, OneHundredTwentyEight, TwoHundredFiftySix, FiveHundredTwelve }
		#endregion
		#region Serialized Variables
		[Header("Pre Update / Initial Settings")]
		[SerializeField] private GameObject objectPrefab;
		[SerializeField] private FactorsOfTwo visualizedObjects;
		[SerializeField] private float freqbandHighest = 0f;
		[Header("Updated Settings")]
		[Space]
		[Header("General")]
		[SerializeField] private Channel channel;
		[SerializeField] private VisualizationState audioVisualizationState;
		[Space]
		[Header("Material")]
		[SerializeField] private Color objectColor;
		[SerializeField] private Color emissiveColor;
		[SerializeField] private float emissionIntensity = 1f;
		[SerializeField] private float exposureIntensity = 1f;
		[Space]
		[Header("Object")]
		[SerializeField] private AnimationCurve shapeCurve;
		[SerializeField] private bool editShapeCurveRealtime = false;
		[SerializeField] private float shapeRadius = 7f;
		[Tooltip("DO NOT CHANGE THIS AT THE START")]
		[SerializeField] private int dontRenderHighPitchAmount = 0;
		[SerializeField] private float objectMinHeightX = 0.2f;
		[SerializeField] private float objectMinHeightY = 0.2f;
		[SerializeField] private float objectMinHeightZ = 0.2f;
		[SerializeField] private float objectXScale = 0.1f;
		[SerializeField] private float objectYScale = 0.1f;
		[SerializeField] private float objectZScale = 30f;
		[SerializeField] private float rotationDegrees = 360f;
		[SerializeField] private float spiralAddBaseX = 0.01f;
		[SerializeField] private float spiralAddBaseY = 0.01f;
		[SerializeField] private float spiralAddBaseZ = 0.01f;
		[Space]
		[Header("Other Fx")]
		[SerializeField] private float permanentRotationFactorX = 0f;
		[SerializeField] private float permanentRotationFactorY = 0f;
		[SerializeField] private float permanentRotationFactorZ = 0f;
		[SerializeField] private float rotationSpeed = 50f;
		[Header("Freq Band")]
		[SerializeField] private float freqBandBufferScaler = 100f;
		[Space]
		[Header("Band Buffer")]
		[SerializeField] private float bufferDecreaseFactor = 1.2f;
		[Header("Audio Band Buffer")]
		[SerializeField] private bool useAudioBandBuffer = false;
		[SerializeField] private float audioBandBufferScaler = 0.5f;
		[Space]
		[Header("Amplitude")]
		[SerializeField] private bool useAmplitude = false;
		[SerializeField] private bool useAmplitudeBuffer = false;
		[SerializeField] private float amplitudeScalerX = 0.5f;
		[SerializeField] private float amplitudeScalerY = 0.5f;
		[SerializeField] private float amplitudeScalerZ = 0.5f;
		[SerializeField] private float amplitudeBufferScalerX = 0.5f;
		[SerializeField] private float amplitudeBufferScalerY = 0.5f;
		[SerializeField] private float amplitudeBufferScalerZ = 0.5f;
		[SerializeField] private float minAddedScaleX = 0f;
		[SerializeField] private float minAddedScaleY = 0f;
		[SerializeField] private float minAddedScaleZ = 0f;
		#endregion
		#region Private Variables
		private SampleRateManager sRM;
		private Utility util;
		private AnimationCurve sampleCurve;
		private GameObject[] objectArray;
		private Material[] objectMaterials;
		private float[] freqBand;
		private float[] bandBuffer;
		private float[] bufferDecrease;
		private float[] freqBandHighest;
		private float[] audioBand;
		[HideInInspector] public float[] audioBandBuffer;
		private float amplitude;
		private float amplitudeBuffer;
		private float amplitudeHighest;
		private float oneDividedByObjectCount;
		private int objectCount;
		private bool scaleAlreadyReset = false;
		private float oldShapeRadius;
		private float oldRotationDegrees;
		private int oldDontRenderHighPitchAmount;
		private float currentRotationX;
		private float currentRotationY;
		private float currentRotationZ;
		private float oldSpiralAddBaseX;
		private float oldSpiralAddBaseY;
		private float oldSpiralAddBaseZ;
		#endregion

		#region Monobehaviour
		void Update()
		{
			// update Object scales
			switch (audioVisualizationState)
			{
				case VisualizationState.FrequencyBands:
					SetObjectInformation();
					FrequencyBands();
					BandBuffer();
					AudioBands();
					if (useAmplitude)
						GetAmplitude();
					break;
				case VisualizationState.AudioBandBuffer:
					SetObjectInformation();
					FrequencyBands();
					BandBuffer();
					AudioBands();
					if (useAmplitude)
						GetAmplitude();
					break;
				default:
					break;
			}

			// other Fx
			if (!editShapeCurveRealtime)
				RotateBase();
		}
		#endregion

		#region Initialization
		public void Init()
		{
			sRM = GameObject.Find("SampleRateManager").GetComponent<SampleRateManager>();
			sampleCurve = sRM.SwitchAnimationCurve(visualizedObjects);

			// set array lenghts
			objectCount = Utility.SwitchFactorsOfTwoFull(visualizedObjects);

			objectArray = new GameObject[objectCount];
			objectMaterials = new Material[objectCount];
			freqBand = new float[objectCount];
			bandBuffer = new float[objectCount];
			bufferDecrease = new float[objectCount];
			freqBandHighest = new float[objectCount];
			audioBand = new float[objectCount];
			audioBandBuffer = new float[objectCount];

			// set backup values
			oneDividedByObjectCount = 1f / objectArray.Length;
			oldShapeRadius = shapeRadius;
			oldRotationDegrees = rotationDegrees;
			oldDontRenderHighPitchAmount = dontRenderHighPitchAmount;
			oldSpiralAddBaseX = spiralAddBaseX;
			oldSpiralAddBaseY = spiralAddBaseY;
			oldSpiralAddBaseZ = spiralAddBaseZ;

			// create objects in shape
			float spiralAddX = spiralAddBaseX;
			float spiralAddY = spiralAddBaseY;
			float spiralAddZ = spiralAddBaseZ;
			for (int i = 0; i < objectArray.Length; i++)
			{
				GameObject sampleCubeInst = (GameObject)Instantiate(objectPrefab);
				sampleCubeInst.transform.position = this.transform.position;
				sampleCubeInst.transform.parent = this.transform;
				sampleCubeInst.name = "Cube " + i;
				this.transform.eulerAngles = new Vector3(0f, (rotationDegrees / objectArray.Length) * i, 0f);
				sampleCubeInst.transform.position = Vector3.forward * shapeRadius * shapeCurve.Evaluate(i * oneDividedByObjectCount) + new Vector3(
					spiralAddX,
					spiralAddY,
					spiralAddZ
					);
				spiralAddX += spiralAddBaseX;
				spiralAddY += spiralAddBaseY;
				spiralAddZ += spiralAddBaseZ;
				sampleCubeInst.transform.LookAt(this.transform);
				objectArray[i] = sampleCubeInst;
				objectMaterials[i] = sampleCubeInst.GetComponent<MeshRenderer>().material;
				objectMaterials[i].SetColor("_BaseColor", objectColor);
				objectMaterials[i].SetColor("_EmissiveColor", emissiveColor);
			}
			// reset base object
			transform.rotation = Quaternion.identity;

			// audio profile
			for (int i = 0; i < objectArray.Length; i++)
			{
				freqBandHighest[i] = freqbandHighest;
			}
		}
		#endregion

		#region Updated
		void SetObjectInformation()
		{
			// reset local scale
			if (!scaleAlreadyReset && !useAmplitude)
				ResetLocalScale();

			// set objectPositions
			if (oldShapeRadius != shapeRadius || oldRotationDegrees != rotationDegrees || editShapeCurveRealtime || spiralAddBaseX != oldSpiralAddBaseX || spiralAddBaseY != oldSpiralAddBaseY || spiralAddBaseZ != oldSpiralAddBaseZ)
				SetPositions();

			// change amount of cubes that are not rendered
			if (dontRenderHighPitchAmount != oldDontRenderHighPitchAmount)
			{
				DontRender();
				SetPositions();
			}
		}
		void FrequencyBands()
		{
			int count = 0;
			for (int i = 0; i < freqBand.Length; i++)
			{
				float average = 0;
				//int sampleCount = (int)Mathf.Pow(2, i) * 2;
				int sampleCount = (int)sampleCurve.Evaluate(i);

				for (int j = 0; j < sampleCount; j++)
				{
					switch (channel)
					{
						case Channel.Stereo:
							average += (sRM.samplesLeft[count] + sRM.samplesRight[count]) * (count + 1);
							break;
						case Channel.Left:
							average += sRM.samplesLeft[count] * (count + 1);
							break;
						case Channel.Right:
							average += sRM.samplesRight[count] * (count + 1);
							break;
						default:
							break;
					}
					count++;
				}

				average /= count;

				freqBand[i] = average * freqBandBufferScaler;
			}

			if (audioVisualizationState == VisualizationState.FrequencyBands)
			{
				for (int i = 0; i < objectArray.Length/* - dontRenderHighPitchAmount*/; i++)
				{
					objectArray[i].transform.localScale = new Vector3(
						freqBand[i] * objectXScale + objectMinHeightX,
						freqBand[i] * objectYScale + objectMinHeightY,
						freqBand[i] * objectZScale + objectMinHeightZ
						);
				}
			}
		}
		void BandBuffer()
		{
			for (int i = 0; i < bandBuffer.Length; i++)
			{
				if (freqBand[i] > bandBuffer[i])
				{
					bandBuffer[i] = freqBand[i];
					bufferDecrease[i] = 0.005f;
				}
				else if (freqBand[i] <= bandBuffer[i])
				{
					bandBuffer[i] -= bufferDecrease[i];
					bufferDecrease[i] *= bufferDecreaseFactor;
				}
			}
		}
		void AudioBands()
		{
			// calculate audioBands
			for (int i = 0; i < audioBand.Length; i++)
			{
				if (freqBand[i] > freqBandHighest[i])
				{
					freqBandHighest[i] = freqBand[i];
				}
				audioBand[i] = (freqBand[i] / freqBandHighest[i]);
				audioBandBuffer[i] = (bandBuffer[i] / freqBandHighest[i]);
			}

			// set scales according to the mode
			if (audioVisualizationState == VisualizationState.AudioBandBuffer)
			{
				if (useAudioBandBuffer)
				{
					for (int i = 0; i < objectArray.Length/* - dontRenderHighPitchAmount*/; i++)
					{
						objectArray[i].transform.localScale = new Vector3(
							audioBandBuffer[i] * objectXScale * audioBandBufferScaler + objectMinHeightX,
							audioBandBuffer[i] * objectYScale * audioBandBufferScaler + objectMinHeightY,
							audioBandBuffer[i] * objectZScale * audioBandBufferScaler + objectMinHeightZ
							);
					}
				}
				else
				{
					for (int i = 0; i < objectArray.Length/* - dontRenderHighPitchAmount*/; i++)
					{
						objectArray[i].transform.localScale = new Vector3(
							audioBand[i] * objectXScale * audioBandBufferScaler + objectMinHeightX,
							audioBand[i] * objectYScale * audioBandBufferScaler + objectMinHeightY,
							audioBand[i] * objectZScale * audioBandBufferScaler + objectMinHeightZ
							);
					}
				}
			}

			// calculate colors for every mode
			for (int i = 0; i < objectArray.Length; i++)
			{
				if (useAudioBandBuffer)
				{
					Color color = new Color(audioBandBuffer[i], audioBandBuffer[i], audioBandBuffer[i]) * emissionIntensity;
					SetObjectColors(objectMaterials[i], objectColor, color, i);
				}
				else
				{
					Color color = new Color(audioBand[i], audioBand[i], audioBand[i]) * emissionIntensity;
					SetObjectColors(objectMaterials[i], objectColor, color, i);
				}
			}
		}
		void GetAmplitude()
		{
			float currentAmplitude = 0f;
			float currentAmplitudeBuffer = 0f;

			for (int i = 0; i < audioBand.Length; i++)
			{
				currentAmplitude += audioBand[i];
				currentAmplitudeBuffer += audioBandBuffer[i];
			}

			if (currentAmplitude > amplitudeHighest)
			{
				amplitudeHighest = currentAmplitude;
			}
			amplitude = currentAmplitude / amplitudeHighest;
			amplitudeBuffer = currentAmplitudeBuffer / amplitudeHighest;

			if (useAmplitudeBuffer)
			{
				transform.localScale = new Vector3(
					amplitudeBuffer * amplitudeBufferScalerX + minAddedScaleX,
					amplitudeBuffer * amplitudeBufferScalerY + minAddedScaleY,
					amplitudeBuffer * amplitudeBufferScalerZ + minAddedScaleZ
					);
			}
			else
			{
				transform.localScale = new Vector3(
					amplitude * amplitudeScalerX + minAddedScaleX,
					amplitude * amplitudeScalerY + minAddedScaleY,
					amplitude * amplitudeScalerZ + minAddedScaleZ
					);
			}

			scaleAlreadyReset = false;
		}
		#endregion

		#region other Fx
		void RotateBase()
		{
			transform.Rotate(
				new Vector3(
				permanentRotationFactorX,
				permanentRotationFactorY,
				permanentRotationFactorZ
				),
				rotationSpeed * Time.deltaTime
				);

			currentRotationX = transform.rotation.x;
			currentRotationY = transform.rotation.y;
			currentRotationZ = transform.rotation.z;
		}
		void SetObjectColors(Material objectColor, Color baseColor, Color emissiveColor, int i)
		{
			objectColor.SetColor("_BaseColor", baseColor);
			objectColor.SetColor("_EmissiveColor", emissiveColor);
			objectColor.SetFloat("_EmissiveExposureWeight", audioBand[i] * exposureIntensity);
		}
		#endregion

		#region SetObjectInformation Methods
		private void ResetLocalScale()
		{
			transform.localScale = Vector3.one;
			scaleAlreadyReset = true;
		}
		private void SetPositions()
		{
			float spiralAddX = spiralAddBaseX;
			float spiralAddY = spiralAddBaseY;
			float spiralAddZ = spiralAddBaseZ;
			for (int i = 0; i < objectArray.Length; i++)
			{
				this.transform.eulerAngles = new Vector3(currentRotationX, currentRotationY + (rotationDegrees / (objectArray.Length - dontRenderHighPitchAmount)) * i, currentRotationZ);
				objectArray[i].transform.position = Vector3.forward * shapeRadius * shapeCurve.Evaluate(i * oneDividedByObjectCount) + new Vector3(
					spiralAddX,
					spiralAddY,
					spiralAddZ
					);
				spiralAddX += spiralAddBaseX;
				spiralAddY += spiralAddBaseY;
				spiralAddZ += spiralAddBaseZ;
				objectArray[i].transform.LookAt(this.transform);
			}
			oldShapeRadius = shapeRadius;
			oldRotationDegrees = rotationDegrees;
			oldSpiralAddBaseX = spiralAddBaseX;
			oldSpiralAddBaseY = spiralAddBaseY;
			oldSpiralAddBaseZ = spiralAddBaseZ;
		}
		private void DontRender()
		{
			for (int i = 0; i < objectArray.Length; i++)
			{
				if (i >= (objectArray.Length - 1) - dontRenderHighPitchAmount)
				{
					objectArray[i].SetActive(false);
				}
				else
				{
					objectArray[i].SetActive(true);
				}
			}
			oldDontRenderHighPitchAmount = dontRenderHighPitchAmount;
			oneDividedByObjectCount = 1f / (objectArray.Length - dontRenderHighPitchAmount);
		}
		#endregion
	}
}
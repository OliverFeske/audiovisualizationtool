﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundAudioVisualizationManager : MonoBehaviour
{
	[SerializeField] private AudioSource source;
	[SerializeField] private float scaleFactor;
	[SerializeField] private float sampleRate = 0.1f;
	[SerializeField] private float smoothing = 60f;
	[SerializeField] private float maxScale = 5f;
	[SerializeField] private List<Transform> amplitudeCubeTransforms;
	private float[] samples = new float[512];
	private float timer;
	private float debugIdx;
	private float sum;
	private float upCounter = 0;

	private void Start()
	{
		sum = 0;
		foreach (Transform child in transform)
		{
			amplitudeCubeTransforms.Add(child);
			upCounter++;
			sum += upCounter;
			Debug.Log(sum);
		}
	}

	void Update()
	{
		timer += Time.deltaTime;
		if (timer > sampleRate)
		{
			//SourceOutput();
			SpectrumOutput();
			timer = 0f;
		}
	}

	void SourceOutput()
	{
		timer += Time.deltaTime;

		if (timer > sampleRate)
		{
			float amp = 0;
			source.GetOutputData(samples, 0);
			for (int i = 0; i < samples.Length; i++)
			{
				amp += Mathf.Abs(samples[i]);
			}
			amp /= samples.Length;
			amp *= scaleFactor;

			Vector3 rawscale = new Vector3(transform.localScale.x, amp, transform.localScale.z);
			transform.localScale = Vector3.Lerp(transform.localScale, rawscale, smoothing * Time.deltaTime);
			timer = 0f;
		}
	}
	void SpectrumOutput()
	{
		float samplesPerUse = 0;
		float addedIdx = 0;
		int aCTcount = amplitudeCubeTransforms.Count;
		source.GetSpectrumData(samples, 0, FFTWindow.Blackman);
		// (sum of all objects added together / num of objects) * i + 1 
		// so the first object only takes 1 line
		// the second takes 

		// for heights
		for (int i = 0; i < amplitudeCubeTransforms.Count; i++)
		{
			float amp = 0;
			samplesPerUse = samples.Length / aCTcount;

			for (int j = (int)addedIdx; j < addedIdx + samplesPerUse; j++)
			{
				amp += Mathf.Abs(samples[j]);
			}
			addedIdx += samplesPerUse;

			amp /= samples.Length / aCTcount;
			amp *= scaleFactor;
			amp = Mathf.Clamp(amp, 0, maxScale);

			Vector3 childScale = amplitudeCubeTransforms[i].localScale;
			Vector3 rawscale = new Vector3(childScale.x, amp, childScale.z);
			childScale = Vector3.Lerp(childScale, rawscale, smoothing * Time.deltaTime);
			amplitudeCubeTransforms[i].localScale = childScale;
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class ScreenRecorder : MonoBehaviour
{
	#region References
	[SerializeField] PresetManager presetManager;
	#endregion
	#region Public Variables
	[Space]
	public Format CurFormat = Format.PPM;

	[Space]
	public string ScreenShotFolder = "Screenshots";
	public int CaptureWidth = 1920;
	public int CaptureHeight = 1080;
	public bool OptimizeForManyScreenshots = true;
	public bool TakeScreenShot;

	[HideInInspector] public string PresetName;
	#endregion
	#region Private Variables
	[Space]
	[SerializeField] GameObject hideUI;
	[SerializeField] Camera mainCam;
	Rect rect;
	RenderTexture renderTexture;
	Texture2D screenShot;
	[SerializeField] string folder;
	int counter;
	#endregion

	#region MonoBehaviour
	private void Awake()
	{
		if (folder == null || folder.Length == 0)
		{
			folder = Application.persistentDataPath + "/" + ScreenShotFolder + "/";

			Directory.CreateDirectory(folder);
		}
	}
	void LateUpdate()
	{
		if (TakeScreenShot) { CaptureScreenShot(); }
	}
	#endregion
	#region Public Methods
	void CaptureScreenShot()
	{
		TakeScreenShot = false;

		if(hideUI != null) { hideUI.SetActive(false); }

		if(renderTexture == null)
		{
			rect = new Rect(0, 0, CaptureWidth, CaptureHeight);
			renderTexture = new RenderTexture(CaptureWidth, CaptureHeight, 24);
			screenShot = new Texture2D(CaptureWidth, CaptureHeight, TextureFormat.RGB24, false);
		}

		mainCam.targetTexture = renderTexture;

		mainCam.Render();

		RenderTexture.active = renderTexture;
		screenShot.ReadPixels(rect, 0, 0);

		mainCam.targetTexture = null;
		RenderTexture.active = null;

		string filename = UniqueFileName((int)rect.width, (int)rect.height);

		byte[] fileHeader = null;
		byte[] fileData = null;

		switch (CurFormat)
		{
			case Format.RAW:
				fileData = screenShot.GetRawTextureData();
				break;
			case Format.JPG:
				fileData = screenShot.EncodeToJPG();
				break;
			case Format.PNG:
				fileData = screenShot.EncodeToPNG();
				break;
			case Format.PPM:
				string headerString = $"P6\n{rect.width} {rect.height}\n255\n";

				fileHeader = System.Text.Encoding.ASCII.GetBytes(headerString);
				fileData = screenShot.GetRawTextureData();
				break;
			default:
				break;
		}

		new System.Threading.Thread(() =>
		{
			var f = File.Create(filename);
			if (fileHeader != null) { f.Write(fileHeader, 0, fileHeader.Length); }
			f.Write(fileData, 0, fileData.Length);
			f.Dispose();
			f.Close();
			Debug.LogError($"Wrote screenshot {filename} of size {fileData.Length}");
		}).Start();

		if(hideUI != null) { hideUI.SetActive(true); }

		if (OptimizeForManyScreenshots == false)
		{
			Destroy(renderTexture);
			renderTexture = null;
			screenShot = null;
		}

		presetManager.CreatePreset(filename);
	}
	#endregion
	#region Private Methdods
	string UniqueFileName(int width, int height)
	{
		if(folder == null || folder.Length == 0)
		{
			folder = Application.persistentDataPath + "/" + ScreenShotFolder + "/";

			Directory.CreateDirectory(folder);

			string mask = $"screen_{width}x{height}*.{CurFormat.ToString().ToLower()}";
			counter = Directory.GetFiles(folder, mask, SearchOption.TopDirectoryOnly).Length;
		}

		string filename = $"{folder}screen_{width}x{height}_{counter}_{PresetName}.{CurFormat.ToString().ToLower()}";

		counter++;

		return filename;
	}
	#endregion
}

public enum Format { RAW, JPG, PNG, PPM }

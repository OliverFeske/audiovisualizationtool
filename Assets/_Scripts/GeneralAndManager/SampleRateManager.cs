﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SampleRateManager : MonoBehaviour
{
	#region Private Variables
	[SerializeField] List<AnimationCurve> curves;
	[SerializeField] AudioSource source;

	int sampleAmount = 512;
	#endregion
	#region Public Variables / Properties
	public bool ListenToAudio = true;
	[HideInInspector] public float[] samplesLeft;
	[HideInInspector] public float[] samplesRight;
	#endregion

	#region MonoBehaviour
	void Start()
	{
		samplesLeft = new float[sampleAmount];
		samplesRight = new float[sampleAmount];
	}
	void Update()
	{
		if (ListenToAudio)
		{
			// get spectrum data of left and right
			source.GetSpectrumData(samplesLeft, 0, FFTWindow.Blackman);
			source.GetSpectrumData(samplesRight, 1, FFTWindow.Blackman);
		}
	}
	#endregion
	#region Public Methods
	public void ClearArrays()
	{
		for (int i = 0; i < sampleAmount; i++)
		{
			samplesLeft[i] = 0f;
			samplesRight[i] = 0f;
		}
	}
	public AnimationCurve SwitchAnimationCurve(FactorsOfTwo factorsOfTwo)
	{
		int idx = Utility.SwitchFactorsOfTwoMappedToLength(factorsOfTwo);

		return curves[idx];
	}
	#endregion
}
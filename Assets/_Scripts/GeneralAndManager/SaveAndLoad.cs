﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class SaveAndLoad : MonoBehaviour
{
	#region References
	[SerializeField] AudioItemManager manager;
	[SerializeField] CurrentItemUI cIUI;
	BinarySerializer serializer;
	SaveData data;
	#endregion
	#region Private Variables
	[SerializeField] GameObject itemHolder;
	[SerializeField] GameObject itemHolderUI;
	[SerializeField] string gameDataFolderName = "Saves";
	#endregion
	#region Public Variables
	public string SavesPath;
	#endregion

	#region MonoBehaviour
	void Awake()
	{
		serializer = GetComponent<BinarySerializer>();

		// if the saves folder does not exist, create or fill it
		if (SavesPath == null || SavesPath.Length == 0)
		{
			SavesPath = Application.persistentDataPath + "/" + gameDataFolderName + "/";

			Directory.CreateDirectory(SavesPath);
		}
	}
	#endregion
	#region Public Methods (Save / Load)
	public void Save(string filename, string imagepath)
	{
		List<SaveItem> itemList = new List<SaveItem>();

		AudioItem[] items = itemHolder.GetComponentsInChildren<AudioItem>();

		SaveItem[] itemArray = new SaveItem[items.Length];

		for (int i = 0; i < items.Length; i++)
		{
			SaveItem newItem = new SaveItem(items[i], imagepath);
			itemList.Add(newItem);
		}

		data = new SaveData(itemList);

		serializer.SaveGameData(SavesPath, filename, data);
	}
	public void Load(string filename)
	{
		AudioItemListUIItem[] items = itemHolderUI.GetComponentsInChildren<AudioItemListUIItem>();

		if (items.Length != 0) { manager.ClearScene(items); }

		data = serializer.LoadGameData(SavesPath, filename);

		for (int i = 0; i < data.SaveItemList.Count; i++)
		{
			var newItem = manager.AddOldItem();

			SaveItem curItem = data.SaveItemList[i];

			newItem.OverrideValues(curItem);

			if (i == data.SaveItemList.Count - 1) { cIUI.SetValues(newItem); }

			newItem.AILUII.SetName(newItem.ItemName);
			newItem.AILUII.SelectItem();
		}

		for (int i = 0; i < items.Length; i++)
		{
			items[i].NameField.text = items[i].Item.ItemName;
		}
	}
	#endregion
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
	#region Public UI Methods
	// Collapse UI
	public void CollapseRight(CollapseButton button)
	{
		if (button.Rect == null) { return; }

		Vector3 offset = new Vector3(button.Rect.rect.width, 0f, 0f);

		if (button.IsCollapsed)
		{
			button.Rect.localPosition -= offset;
			button.IsCollapsed = false;
		}
		else if (!button.IsCollapsed)
		{
			button.Rect.localPosition += offset;
			button.IsCollapsed = true;
		}
	}
	public void CollapseLeft(CollapseButton button)
	{
		if (button.Rect == null) { return; }

		Vector3 offset = new Vector3(button.Rect.rect.width, 0f, 0f);

		if (button.IsCollapsed)
		{
			button.Rect.localPosition += offset;
			button.IsCollapsed = false;
		}
		else if (!button.IsCollapsed)
		{
			button.Rect.localPosition -= offset;
			button.IsCollapsed = true;
		}
	}
	public void CollapseUp(CollapseButton button)
	{
		if (button.Rect == null) { return; }

		Vector3 offset = new Vector3(0f, button.Rect.rect.height, 0f);

		if (button.IsCollapsed)
		{
			button.Rect.localPosition -= offset;
			button.IsCollapsed = false;
		}
		else if (!button.IsCollapsed)
		{
			button.Rect.localPosition += offset;
			button.IsCollapsed = true;
		}
	}
	public void CollapseDown(CollapseButton button)
	{
		if (button.Rect == null) { return; }

		Vector3 offset = new Vector3(0f, button.Rect.rect.height, 0f);

		if (button.IsCollapsed)
		{
			button.Rect.localPosition += offset;
			button.IsCollapsed = false;
		}
		else if (!button.IsCollapsed)
		{
			button.Rect.localPosition -= offset;
			button.IsCollapsed = true;
		}
	}

	// Open and Close UI
	public void TogglePanel(GameObject go)
	{
		if (go.activeSelf) { go.SetActive(false); }
		else { go.SetActive(true); }
	}
	#endregion
}
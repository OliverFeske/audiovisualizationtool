﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class AudioItemListUIItem : MonoBehaviour
{
	#region References
	AudioItemListUI aILUI;
	CurrentItemUI cIUI;
	#endregion
	#region Public Variables / Properties
	public TMP_InputField NameField;
	AudioItem item;
	public AudioItem Item { get => item; }
	#endregion

	#region InitItem
	public void Init(AudioItem _item, AudioItemListUI _aILUI, CurrentItemUI _cIUI)
	{
		item = _item;
		aILUI = _aILUI;
		cIUI = _cIUI;

		NameField.text = item.ItemName;
	}
	#endregion
	#region Public UI Methods
	public void ChangeName()
	{
		item.ItemName = NameField.text;
		cIUI.ChangeName(NameField.text);
	}
	public void SetName(string name)
	{
		NameField.text = name;
	}
	public void DestroyItem()
	{
		aILUI.AskToDestroy(this);
	}
	public void SelectItem()
	{
		aILUI.SelectCurrentItem(item);
	}
	public void DefinitelyDestroy()
	{
		aILUI.SetItemKey(Item.ItemKey);
		aILUI.RemoveAudioItem();
	}
	#endregion
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CurrentItemUI : MonoBehaviour
{
	const string nullString = "";
	const float nullFloat = 0f;
	const int nullInt = 0;

	[Header("Script References")]
	[SerializeField] AudioItemListUI aILUI;

	[Space]
	[Header("Header - Item Name")]
	[SerializeField] TMP_Text itemName;

	[Space]
	[Header("Material Settings")]
	[SerializeField] InputField emissiveColorStrength;
	[SerializeField] InputField emissiveExposureStrength;
	[SerializeField] Slider sliderR;
	[SerializeField] Slider sliderG;
	[SerializeField] Slider sliderB;
	[SerializeField] TMP_Text sliderRText;
	[SerializeField] TMP_Text sliderGText;
	[SerializeField] TMP_Text sliderBText;
	[SerializeField] Image colorImage;

	[Space]
	[Header("Cluster Settings")]
	[SerializeField] InputField offsetX;
	[SerializeField] InputField offsetY;
	[SerializeField] InputField offsetZ;
	[SerializeField] InputField clusterRotationX;
	[SerializeField] InputField clusterRotationY;
	[SerializeField] InputField clusterRotationZ;
	[SerializeField] InputField clusterSpacing;
	[SerializeField] TMP_Dropdown visualizationStateDD;
	[SerializeField] TMP_Dropdown objectAmountDD;
	[SerializeField] TMP_Dropdown channelDD;

	[Space]
	[Header("Object Settings")]
	[SerializeField] InputField objectMinHeightX;
	[SerializeField] InputField objectMinHeightY;
	[SerializeField] InputField objectMinHeightZ;
	[SerializeField] InputField objectRotationX;
	[SerializeField] InputField objectRotationY;
	[SerializeField] InputField objectRotationZ;
	[SerializeField] InputField objectScalesX;
	[SerializeField] InputField objectScalesY;
	[SerializeField] InputField objectScalesZ;
	[SerializeField] InputField bufferDecreaseDefault;
	[SerializeField] InputField bufferDecreaseFactor;

	#region Public Methods
	public void SetValues(AudioItem item)
	{
		// Item Name
		itemName.text = item.ItemName;

		// Material Settings
		emissiveColorStrength.text = item.EmissiveColorStrength.ToString();
		emissiveExposureStrength.text = item.EmissiveExposureStrength.ToString();
		sliderR.value = (int)(item.BaseColor.r * 255f);
		sliderG.value = (int)(item.BaseColor.g * 255f);
		sliderB.value = (int)(item.BaseColor.b * 255f);
		sliderRText.text = sliderR.value.ToString();
		sliderGText.text = sliderG.value.ToString();
		sliderBText.text = sliderB.value.ToString();
		colorImage.color = item.BaseColor;

		// Cluster Settings
		offsetX.text = item.Offset.x.ToString();
		offsetY.text = item.Offset.y.ToString();
		offsetZ.text = item.Offset.z.ToString();
		clusterRotationX.text = item.ClusterRotation.x.ToString();
		clusterRotationY.text = item.ClusterRotation.y.ToString();
		clusterRotationZ.text = item.ClusterRotation.z.ToString();
		clusterSpacing.text = item.Spacing.ToString();
		visualizationStateDD.value = (int)item.AudioVisualizationState;
		objectAmountDD.value = (int)item.ObjectAmount;
		channelDD.value = (int)item.CurChannel;

		// Object Settings
		objectMinHeightX.text = item.ObjectMinHeight.x.ToString();
		objectMinHeightY.text = item.ObjectMinHeight.y.ToString();
		objectMinHeightZ.text = item.ObjectMinHeight.y.ToString();
		objectRotationX.text = item.ObjectRotation.x.ToString();
		objectRotationY.text = item.ObjectRotation.y.ToString();
		objectRotationZ.text = item.ObjectRotation.z.ToString();
		objectScalesX.text = item.ObjectScale.x.ToString();
		objectScalesY.text = item.ObjectScale.y.ToString();
		objectScalesZ.text = item.ObjectScale.z.ToString();
		bufferDecreaseDefault.text = item.BufferDecreaseDefault.ToString();
		bufferDecreaseFactor.text = item.BufferDecreaseFactor.ToString();
	}
	public void SetNullValues()
	{
		// Item Name
		itemName.text = nullString;

		// Material Settings
		emissiveColorStrength.text = nullString;
		emissiveExposureStrength.text = nullString;
		sliderR.value = nullFloat;
		sliderG.value = nullFloat;
		sliderB.value = nullFloat;
		sliderRText.text = nullString;
		sliderGText.text = nullString;
		sliderBText.text = nullString;
		colorImage.color = Color.white;

		// Cluster Settings
		offsetX.text = nullString;
		offsetY.text = nullString;
		offsetZ.text = nullString;
		clusterRotationX.text = nullString;
		clusterRotationY.text = nullString;
		clusterRotationZ.text = nullString;
		clusterSpacing.text = nullString;
		visualizationStateDD.value = nullInt;
		objectAmountDD.value = nullInt;
		channelDD.value = nullInt;

		// Object Settings
		objectMinHeightX.text = nullString;
		objectMinHeightY.text = nullString;
		objectMinHeightZ.text = nullString;
		objectRotationX.text = nullString;
		objectRotationY.text = nullString;
		objectRotationZ.text = nullString;
		objectScalesX.text = nullString;
		objectScalesY.text = nullString;
		objectScalesZ.text = nullString;
		bufferDecreaseDefault.text = nullString;
		bufferDecreaseFactor.text = nullString;
	}
	public void ChangeName(string name) { itemName.text = name; }
	public void ChangeTextR() { sliderRText.text = sliderR.value.ToString(); }
	public void ChangeTextG() { sliderGText.text = sliderG.value.ToString(); }
	public void ChangeTextB() { sliderBText.text = sliderB.value.ToString(); }
	public void ChangeColorProperties(Color color)
	{
		colorImage.color = color;
		sliderRText.text = ((int)(color.r * 255f)).ToString();
		sliderGText.text = ((int)(color.g * 255f)).ToString();
		sliderBText.text = ((int)(color.b * 255f)).ToString();
	}
	#endregion
}